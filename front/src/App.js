import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import './App.css';
import {Line} from 'react-chartjs-2';
import "react-datepicker/dist/react-datepicker.css";
import Dropdown from 'react-dropdown'
import 'react-dropdown/style.css'
const data = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July','b','n','j','u','t'],
  datasets: [
    {
      label: 'My First dataset',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(75,192,192,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [65, 59, 80, 81, 56, 55, 40,2,100,-100]
    }
  ]
};
const options = [
  'one', 'two', 'three'
]


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date(),
      defaultOption:'one'
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSelect = this.onSelect.bind(this);
  }
  handleChange(date) {
    console.log(date)
    this.setState({
      startDate: date
    });
  }
  onSelect(el){
    console.log(el)
    this.setState({
      defaultOption: el
    }); }

  render() {
    console.log(this.state.startDate)
    return (
      <div>
        <Dropdown options={options} onChange={this.onSelect} value={this.state.defaultOption} placeholder="Select an option" />
       <DatePicker selected={this.state.startDate}  onChange={this.handleChange}
    dateFormat="MMMM d, yyyy h:mm aa"/>
       <Line data={data} />

      </div>
    );
  }
}

export default App;
