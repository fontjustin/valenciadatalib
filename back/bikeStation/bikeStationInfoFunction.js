const https = require('https');
const mongoose = require('mongoose');
ObjectId = require('mongodb').ObjectID;
const bikeStationInfoModel =  require('../model').bikeStationInfoModel;

const getStation = ()=> https.get('https://api.jcdecaux.com/vls/v1/stations?contract=Valence&apiKey='+process.env.API_KEY, (resp) => {
  let data = '';

  // A chunk of data has been recieved.
  resp.on('data', (chunk) => {
    data += chunk;
  });

  // The whole response has been received. Print out the result.
  resp.on('end', () => {

    data = JSON.parse(data) 
    data.map((station,i)=>{
        delete station["available_bike_stands"];
        delete station["available_bikes"];
       // if  (i)  {console.log(typeof station.number.toString())};
        bikeStationInfoModel.create(station,function (err) {
          if (err) { console.log(err); throw err;}
        });
        if(i==data.length-1){console.log('finish update station info')
      }
      });
            // console.log(JSON.parse(data));
  });

}).on("error", (err) => {
  console.log("Error: " + err.message);
});
const checkIfExist = ()=> {bikeStationInfoModel.find( function (err, docs) {
if(docs.length<1){
 getStation();
}else console.log('bikeStationInfoModel allready exist')
})}
checkIfExist();
//bikeStationInfoModel.remove({},checkIfExist)