const https = require('https');
const mongoose = require('mongoose');
ObjectId = require('mongodb').ObjectID;
const bikeStationModel =  require('../model').bikeStationModel;

const getStation = ()=> https.get('https://api.jcdecaux.com/vls/v1/stations?contract=Valence&apiKey='+process.env.API_KEY, (resp) => {
  let data = '';

  // A chunk of data has been recieved.
  resp.on('data', (chunk) => {
    data += chunk;
  });

  // The whole response has been received. Print out the result.
  resp.on('end', () => {

    data = JSON.parse(data) 
    data.map((station,i)=>{
        bikeStationModel.create(station,function (err) {
          if (err) { console.log(err); throw err;}
        });
        if(i==data.length-1){console.log('finish update station')
      }
      });
  });

}).on("error", (err) => {
  console.log("Error: " + err.message);
});
console.log('start station')
setInterval(function(){getStation() }, 10*30*1000);
