bikeStationModel = require('./bikeStation').bikeStationModel;
bikeStationInfoModel = require('./bikeStationInfo').bikeStationInfoModel;

module.exports = {
    bikeStationModel,
    bikeStationInfoModel
};