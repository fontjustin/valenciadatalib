const mongoose = require('mongoose');


const bikeStationInfoSchema = mongoose.Schema({
    number: { type: Number, default: null },
    contract_name: { type: String, default: null },
    name: { type: String, default: null },
    address: { type: String, default: null },
    position: { type: JSON, default: {} },
    banking: { type: Boolean, default: null },
    bonus: { type: Boolean, default: null },
    bike_stands: { type: Number, default: null },
    status: { type: String, default: null },
    last_update: { type: Number, default: null }

},{ versionKey: false, minimize: false });

const bikeStationInfoModel = mongoose.model('bikeStationInfo', bikeStationInfoSchema);

module.exports = {
  bikeStationInfoModel
};