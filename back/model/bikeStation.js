const mongoose = require('mongoose');


const bikeStationSchema = mongoose.Schema({
    number: { type: Number, default: null },
    available_bike_stands: { type: Number, default: null },
    available_bikes: { type: Number, default: null },
    last_update: { type: Number, default: null }

},{ versionKey: false, minimize: false });

const bikeStationModel = mongoose.model('bikeStation', bikeStationSchema);

module.exports = {
  bikeStationModel
};
